<?xml version="1.0" encoding="UTF-8"?>
<schemalist>
  <schema path="/org/gnome/desktop/a11y/keyboard/" id="org.gnome.desktop.a11y.keyboard">
    <key type="b" name="enable">
      <default>false</default>
      <summary>Enable accessibility keyboard shortcuts</summary>
    </key>
    <key type="b" name="feature-state-change-beep">
      <default>false</default>
      <summary>Beep when a keyboard accessibility feature changes</summary>
      <description>Whether to beep when a keyboard accessibility feature is enabled or disabled.</description>
    </key>
    <key type="b" name="timeout-enable">
      <default>false</default>
      <summary>Disable keyboard accessibility after a timeout</summary>
      <description>Whether to disable keyboard accessibility after a timeout, useful for shared machines.</description>
    </key>
    <key type="i" name="disable-timeout">
      <default>200</default>
      <summary>Duration of the disabling timeout</summary>
      <description>Duration of the timeout before disabling the keyboard accessibility.</description>
    </key>
    <key type="b" name="bouncekeys-enable">
      <default>false</default>
      <summary>Enable 'Bounce Keys'</summary>
      <description>Whether the 'Bounce Keys' keyboard accessibility feature is turned on.</description>
    </key>
    <key type="i" name="bouncekeys-delay">
      <default>300</default>
      <summary>Minimum interval in milliseconds</summary>
      <description>Ignore multiple presses of the same 